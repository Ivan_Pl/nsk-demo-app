package com.pl.nskdemoapp.ui.linearChartView

import android.content.Context
import android.graphics.*
import android.graphics.Paint.Style
import android.os.Parcelable
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Scroller
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.utils.extensions.dpToPx

class LinearChartView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    View(context, attrs, defStyleAttr) {

    private val mGestureDetector: GestureDetector = GestureDetector(context, LinearChartGestureDetector())
    private val mScroller: Scroller = Scroller(context, LinearInterpolator())

    private var dotRadius = dpToPx(3).toFloat()
    private var contentRect: Rect = Rect()
    private var chartWidth: Int = 0
    private var stepX = 0.0
    private var stepY = 0.0

    private var minMaxX: Pair<Double, Double> = 0.0 to 0.0
    private var minMaxY: Pair<Double, Double> = 0.0 to 0.0
    private var dX: Double = 0.0
    private var dY: Double = 0.0
    private var currentX = 0

    private var chartPaint: Paint = Paint().apply {
        isAntiAlias = true
        style = Style.STROKE
        color = ContextCompat.getColor(context, R.color.colorPrimary)
        strokeWidth = dpToPx(2).toFloat()
    }

    private var dotPaint: Paint = Paint(chartPaint).apply {
        style = Style.FILL_AND_STROKE
        color = ContextCompat.getColor(context, R.color.colorPrimary)
    }

    private var innerDotPaint: Paint = Paint(dotPaint).apply {
        style = Style.FILL_AND_STROKE
        color = Color.WHITE
    }

    var data: List<Pair<Double, Double>> = emptyList()
        set(value) {
            field = value
            minMaxX = getMinMaxValue(value.map { it.first })
            minMaxY = getMinMaxValue(value.map { it.second })

            dX = minMaxX.second - minMaxX.first
            dY = minMaxY.second - minMaxY.first

            invalidateChartParams()
            invalidate()
        }

    init {
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        contentRect.set(paddingLeft, paddingTop, width - paddingRight, height - paddingBottom)
        invalidateChartParams()
    }

    override fun computeScroll() {
        super.computeScroll()
        if (mScroller.computeScrollOffset()) {
            currentX = mScroller.currX
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        return mGestureDetector.onTouchEvent(event) || super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (data.isEmpty())
            return

        val currentMaxMin = getCurrentMinMaxX()
        drawLine(canvas, currentMaxMin)
    }

    private fun drawLine(canvas: Canvas, currentMaxMin: Pair<Double, Double>) {
        val firstItemPosition = data.indexOfLast { it.first <= currentMaxMin.first }.let { if (it - 1 < 0) 0 else it - 1 }
        val lastItemPosition = data.indexOfFirst { it.first >= currentMaxMin.second }
            .let { if (it <= 0) data.size else if (it + 1 > data.size) it else it + 1 }

        val points = data.subList(firstItemPosition, lastItemPosition)
            .map {
                val dXForPoint = minMaxX.second - it.first
                val dyForPoint = minMaxY.second - it.second
                val x = paddingStart + ((dX - dXForPoint) * stepX) - currentX
                val y = paddingTop + contentRect.height() - (dY - dyForPoint) * stepY
                PointF(x.toFloat(), y.toFloat())
            }

        val path = Path()
        points.forEachIndexed { i, point ->
            if(i == 0) {
                path.moveTo(point.x, point.y)
            } else {
                path.lineTo(point.x, point.y)
            }
        }
        canvas.drawPath(path, chartPaint)
        drawPoints(canvas, points)
    }

    private fun drawPoints(canvas: Canvas, points: List<PointF>) {
        points.forEach { point ->
            canvas.drawCircle(point.x, point.y, dotRadius, dotPaint)
            canvas.drawCircle(point.x, point.y, dotRadius / 2, innerDotPaint)
        }
    }

    private fun getMinMaxValue(values: List<Double>) =
        (values.min() ?: 0.0) to (values.max() ?: 0.0)

    private fun getCurrentMinMaxX(): Pair<Double, Double> {
        val currentMin = minMaxX.first + currentX / stepX
        val currentMax = currentMin + contentRect.width() / stepX
        return currentMin to currentMax
    }

    private fun invalidateChartParams() {
        chartWidth = when {
            data.size < 10 -> contentRect.width()
            data.size < 50 -> contentRect.width() * 2
            else -> contentRect.width() * 3
        }

        stepY = contentRect.height() / dY
        stepX = chartWidth / dX
    }

    private fun canScroll() = currentX < (chartWidth - contentRect.width()) && currentX > 0

    private inner class LinearChartGestureDetector : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
            mScroller.forceFinished(true)
            ViewCompat.postInvalidateOnAnimation(this@LinearChartView)
            return true
        }

        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
            val nextX = currentX + distanceX.toInt()

            currentX = when {
                nextX < 0 -> 0
                nextX > chartWidth - contentRect.width() -> chartWidth - contentRect.width()
                else -> nextX
            }

            if (canScroll())
                ViewCompat.postInvalidateOnAnimation(this@LinearChartView)
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            mScroller.forceFinished(true)
            mScroller.fling(currentX, 0, -velocityX.toInt(), 0, 0, chartWidth - contentRect.width(), 0, 1)
            ViewCompat.postInvalidateOnAnimation(this@LinearChartView)
            return true
        }
    }

}