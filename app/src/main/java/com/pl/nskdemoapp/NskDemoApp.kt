package com.pl.nskdemoapp

import android.app.Application
import com.pl.nskdemoapp.di.AppComponent
import com.pl.nskdemoapp.di.DaggerAppComponent
import com.pl.nskdemoapp.di.modules.AppModule
import timber.log.Timber

class NskDemoApp : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDaggerComponent()
        initTimber()
    }

    private fun initDaggerComponent() {
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}