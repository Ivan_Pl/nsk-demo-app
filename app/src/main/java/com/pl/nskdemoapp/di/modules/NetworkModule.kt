package com.pl.nskdemoapp.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pl.nskdemoapp.BuildConfig
import com.pl.nskdemoapp.data.network.ApiService
import com.pl.nskdemoapp.data.network.RemoteDataSource
import com.pl.nskdemoapp.data.network.RemoteDataSourceImpl
import com.pl.nskdemoapp.utils.network.TrustAllCertificatesManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import javax.net.ssl.*

@Module
class NetworkModule {

    companion object {
        private const val BASE_URL = "https://demo.bankplus.ru/"
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder()
            .create()

    @Provides
    fun provideHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
        val builder = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)

        val socketFactory = getSSLSocketFactory()
        val trustManager = getTrustManager()
        if (socketFactory != null && trustManager != null) {
            builder.sslSocketFactory(socketFactory, trustManager)
            builder.hostnameVerifier { _, _ -> true }
        }

        return builder.build()
    }

    @Provides
    fun provideApi(okHttpClient: OkHttpClient, gson: Gson): ApiService {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiService::class.java)
    }

    @Provides
    fun provideRemoteDataSource(apiService: ApiService): RemoteDataSource {
        return RemoteDataSourceImpl(apiService)
    }

    private fun getSSLSocketFactory(): SSLSocketFactory? {
        return try {
            val trustAllCerts = arrayOf<TrustManager>(TrustAllCertificatesManager())
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            sslContext.socketFactory
        } catch (e: Exception) {
            Timber.e(e)
            null
        }
    }

    private fun getTrustManager(): X509TrustManager? {
        return try {
            val trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(KeyStore.getInstance(KeyStore.getDefaultType()))
            val trustManagers = trustManagerFactory.trustManagers
            check(trustManagers.size == 1 && trustManagers[0] is X509TrustManager) { "Unexpected default trust managers: $trustManagers" }
            trustManagers[0] as X509TrustManager
        } catch (e: NoSuchAlgorithmException) {
            Timber.e(e)
            null
        }
    }

}
