package com.pl.nskdemoapp.di

import com.pl.nskdemoapp.di.modules.AppModule
import com.pl.nskdemoapp.di.modules.NetworkModule
import com.pl.nskdemoapp.presentation.home.HomeFragment
import com.pl.nskdemoapp.presentation.main.MainActivity
import com.pl.nskdemoapp.presentation.points.PointsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class])
interface AppComponent {

    //activities
    fun inject(activity: MainActivity)

    //fragments
    fun inject(fragment: HomeFragment)
    fun inject(fragment: PointsFragment)

}