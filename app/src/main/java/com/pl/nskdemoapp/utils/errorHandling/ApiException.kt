package com.pl.nskdemoapp.utils.errorHandling

import android.content.Context
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.data.network.responces.BaseResponse


class ApiException private constructor(val kind: Kind, message: String?, exception: Throwable) : RuntimeException(message, exception) {

    companion object {

        private const val RESULT_WRONG_PARAMS = -100
        private const val RESULT_UNEXPECTED = -1

        fun create(response: BaseResponse): ApiException {
            return when {
                response.result == RESULT_WRONG_PARAMS -> ApiException(
                    Kind.WRONG_PARAMS,
                    response.message,
                    RuntimeException(response.message)
                )
                response.result == RESULT_UNEXPECTED -> ApiException(
                    Kind.UNEXPECTED,
                    response.message,
                    RuntimeException(response.message)
                )
                else -> ApiException(
                    Kind.UNEXPECTED,
                    null,
                    RuntimeException()
                )
            }
        }
    }

    fun getReadableMessage(context: Context) : String {
        return when {
            !message.isNullOrBlank() -> message
            kind == Kind.WRONG_PARAMS -> context.getString(R.string.wrong_params_entered_error_msg)
            else -> context.getString(R.string.unexpected_error_msg)
        }
    }

    enum class Kind {
        WRONG_PARAMS,
        UNEXPECTED
    }
}
