package com.pl.nskdemoapp.utils.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.inflate(@LayoutRes resId: Int, attach: Boolean = false) =
    LayoutInflater.from(context).inflate(resId, this, attach)

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide(collapse: Boolean = true) {
    visibility = if (collapse) View.GONE else View.INVISIBLE
}

fun View.setVisible(show: Boolean, collapseIfHide: Boolean = true) {
    if (show) {
        show()
    } else {
        hide(collapseIfHide)
    }
}


fun View.dpToPx(dp: Int) = context.dpToPx(dp)