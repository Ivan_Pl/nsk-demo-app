package com.pl.nskdemoapp.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.pl.moxyandroidx.MvpAndroidXFragment
import com.pl.nskdemoapp.R
import kotlinx.android.synthetic.main.v_toolbar.*
import timber.log.Timber

abstract class BaseFragment : MvpAndroidXFragment() {

    protected abstract val layoutId: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutId, container, false)
        view.isClickable = true
        return view
    }

    override fun onStart() {
        super.onStart()
        Timber.tag("Navigation").d("${this.javaClass.simpleName} started")
    }

    fun replaceFragment(fragment: androidx.fragment.app.Fragment, addToBackStack: Boolean = true) {
        (activity as? BaseActivity)?.replaceFragment(fragment, addToBackStack)
    }

    fun setTitle(@StringRes titleId: Int) {
        toolbar?.setTitle(titleId)
    }

    fun enableToolbarBackBtn(@DrawableRes navigationIconId: Int = R.drawable.ic_arrow_back_white_24dp, action: () -> Unit) {
        toolbar?.let {
            it.setNavigationIcon(navigationIconId)
            it.setNavigationOnClickListener { action.invoke() }
        }
    }

    fun goBack() {
        requireActivity().onBackPressed()
    }

}