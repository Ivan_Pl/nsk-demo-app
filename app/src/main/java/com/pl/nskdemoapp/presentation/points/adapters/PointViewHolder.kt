package com.pl.nskdemoapp.presentation.points.adapters

import android.view.View
import android.view.ViewGroup
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.utils.extensions.inflate
import com.pl.nskdemoapp.presentation.base.adapters.SimpleViewHolder
import kotlinx.android.synthetic.main.li_point.view.*

class PointViewHolder(view: View) : SimpleViewHolder<Point>(view) {

    companion object {
        fun create(parent: ViewGroup): PointViewHolder {
            val view = parent.inflate(R.layout.li_point)
            return PointViewHolder(view)
        }
    }

    override fun bind(data: Point) {
        with(itemView) {
            pointXTextView.text = data.x.toString()
            pointYTextView.text = data.y.toString()
        }
    }

}