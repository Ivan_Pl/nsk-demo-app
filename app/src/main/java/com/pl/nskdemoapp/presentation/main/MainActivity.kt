package com.pl.nskdemoapp.presentation.main

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pl.nskdemoapp.NskDemoApp
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.presentation.base.BaseActivity
import com.pl.nskdemoapp.presentation.home.HomeFragment
import javax.inject.Inject

class MainActivity : BaseActivity(), MainMvpView {

    override val fragmentContainerId: Int = R.id.fragmentContainer

    @Inject
    @InjectPresenter
    lateinit var presenter: MainPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        NskDemoApp.component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ac_main)
    }

    override fun showHomeScreen() {
        replaceFragment(HomeFragment.newInstance(), false)
    }

}
