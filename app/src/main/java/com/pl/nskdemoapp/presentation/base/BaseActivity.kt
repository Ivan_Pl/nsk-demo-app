package com.pl.nskdemoapp.presentation.base

import androidx.fragment.app.Fragment
import com.pl.moxyandroidx.MvpAndroidXActivity

abstract class BaseActivity : MvpAndroidXActivity() {

    protected abstract val fragmentContainerId: Int

    fun replaceFragment(fragment: Fragment, addToBackStack: Boolean = true) {
        supportFragmentManager.beginTransaction().apply {
            replace(fragmentContainerId, fragment, fragment.javaClass.simpleName)
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
        }.commit()
    }

}