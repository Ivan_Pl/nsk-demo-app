package com.pl.nskdemoapp.presentation.home

import com.arellomobile.mvp.InjectViewState
import com.pl.nskdemoapp.presentation.base.BasePresenter
import javax.inject.Inject

@InjectViewState
class HomePresenter @Inject constructor() : BasePresenter<HomeMvpView>() {

    fun onLetsGoBtnClicked(pointsCountStr: String) {
        val pointsCount = pointsCountStr.toIntOrNull()
        pointsCount?.let { viewState.showPointsScreen(it) }
            ?: run { viewState.showInvalidValueErrorMsg() }
    }

}