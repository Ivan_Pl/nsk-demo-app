package com.pl.nskdemoapp.presentation.home

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface HomeMvpView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPointsScreen(PointsCount: Int)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showInvalidValueErrorMsg()
}