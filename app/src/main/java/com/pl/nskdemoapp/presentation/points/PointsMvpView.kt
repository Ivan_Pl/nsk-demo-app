package com.pl.nskdemoapp.presentation.points

import androidx.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.utils.errorHandling.ApiException

interface PointsMvpView : MvpView {
    fun showProgress(show: Boolean)
    fun showPoints(points: List<Point>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(exception: ApiException)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(@StringRes messageResId: Int)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeScreen()

    fun showChart(chartData: List<Pair<Double, Double>>)
}