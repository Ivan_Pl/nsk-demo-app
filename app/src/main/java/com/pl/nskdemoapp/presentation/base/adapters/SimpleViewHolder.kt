package com.pl.nskdemoapp.presentation.base.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class SimpleViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(data: T)

}