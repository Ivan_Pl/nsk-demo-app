package com.pl.nskdemoapp.presentation.base.adapters

import androidx.recyclerview.widget.RecyclerView

abstract class SimpleAdapter<T>(val data: ArrayList<T> = ArrayList()) : RecyclerView.Adapter<SimpleViewHolder<T>>() {

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: SimpleViewHolder<T>, position: Int) {
        holder.bind(data[position])
    }

    fun setItems(items: List<T>) {
        data.clear()
        notifyDataSetChanged()
        addItems(items)
    }

    fun addItems(items: List<T>) {
        val insertPosition = data.size
        data.addAll(items)
        notifyItemRangeInserted(insertPosition, items.size)
    }

    fun addItem(item: T, position: Int = data.size) {
        data.add(position, item)
        notifyItemInserted(position)
    }

    fun removeItem(item: T) {
        val itemPosition = data.indexOf(item)
        data.remove(item)
        notifyItemRemoved(itemPosition)
    }
}
