package com.pl.nskdemoapp.presentation.points

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pl.nskdemoapp.NskDemoApp
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.utils.errorHandling.ApiException
import com.pl.nskdemoapp.presentation.base.BaseFragment
import com.pl.nskdemoapp.presentation.points.adapters.PointsAdapter
import com.pl.nskdemoapp.utils.extensions.setVisible
import com.pl.nskdemoapp.utils.extensions.toast
import kotlinx.android.synthetic.main.fr_points.*
import javax.inject.Inject

class PointsFragment : BaseFragment(), PointsMvpView {

    companion object {

        private const val KEY_POINTS_COUNT = "points_count"

        fun newInstance(pointsCount: Int) = PointsFragment().apply {
            arguments = Bundle().apply {
                putInt(KEY_POINTS_COUNT, pointsCount)
            }
        }
    }

    override val layoutId: Int = R.layout.fr_points

    @Inject
    @InjectPresenter
    lateinit var presenter: PointsPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        NskDemoApp.component.inject(this)
        arguments?.let {
            val pointsCount = it.getInt(KEY_POINTS_COUNT)
            presenter.setData(pointsCount)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        enableToolbarBackBtn { presenter.onBackBtnPressed() }
        setTitle(R.string.points_title)
    }

    override fun showProgress(show: Boolean) {
        pointsContentLinearLayout.setVisible(!show)
        pointsProgressBar.setVisible(show)
    }

    override fun showPoints(points: List<Point>) {
        with(pointsRecyclerView) {
            swapAdapter(PointsAdapter(points), false)
            setVisible(true)
        }
    }

    override fun showChart(chartData: List<Pair<Double, Double>>) {
        pointsLinearChartView.data = chartData
    }

    override fun showError(exception: ApiException) {
        requireContext().toast(exception.getReadableMessage(requireContext()))
    }

    override fun showMessage(messageResId: Int) {
        requireContext().toast(messageResId)
    }

    override fun closeScreen() {
        goBack()
    }

}