package com.pl.nskdemoapp.presentation.points

import com.arellomobile.mvp.InjectViewState
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.utils.errorHandling.ApiException
import com.pl.nskdemoapp.data.repositories.points.PointsRepository
import com.pl.nskdemoapp.presentation.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class PointsPresenter @Inject constructor(private val pointsRepository: PointsRepository) :
    BasePresenter<PointsMvpView>() {

    private var pointsCount = 0

    fun setData(pointsCount: Int) {
        this.pointsCount = pointsCount
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        disposables += pointsRepository.getPoints(pointsCount)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgress(true) }
            .doOnEvent { _, _ -> viewState.showProgress(false) }
            .subscribe({
                viewState.showPoints(it)
                viewState.showChart(it.map { it.x to it.y })
            }, {
                Timber.e(it)
                (it as? ApiException)?.also { viewState.showError(it) } ?: run { viewState.showMessage(R.string.unexpected_error_msg) }
                viewState.closeScreen()
            })
    }

    fun onBackBtnPressed() {
        viewState.closeScreen()
    }

}