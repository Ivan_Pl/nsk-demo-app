package com.pl.nskdemoapp.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.pl.nskdemoapp.presentation.base.BasePresenter
import javax.inject.Inject

@InjectViewState
class MainPresenter @Inject constructor(): BasePresenter<MainMvpView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showHomeScreen()
    }

}