package com.pl.nskdemoapp.presentation.home

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.pl.nskdemoapp.NskDemoApp
import com.pl.nskdemoapp.R
import com.pl.nskdemoapp.presentation.base.BaseFragment
import com.pl.nskdemoapp.presentation.points.PointsFragment
import com.pl.nskdemoapp.utils.extensions.toast
import kotlinx.android.synthetic.main.fr_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeMvpView {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override val layoutId: Int = R.layout.fr_home

    @Inject
    @InjectPresenter
    lateinit var presenter: HomePresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        NskDemoApp.component.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(R.string.app_name)
        homeLetsGoButton.setOnClickListener {
            val pointsCount = homePointsCountEditText.text.toString()
            presenter.onLetsGoBtnClicked(pointsCount)
        }
    }

    override fun showInvalidValueErrorMsg() {
        requireContext().toast(R.string.home_invalid_value_msg)
    }

    override fun showPointsScreen(PointsCount: Int) {
        replaceFragment(PointsFragment.newInstance(PointsCount))
    }

}