package com.pl.nskdemoapp.presentation.points.adapters

import android.view.ViewGroup
import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.presentation.base.adapters.SimpleAdapter

class PointsAdapter(data: List<Point>) : SimpleAdapter<Point>(ArrayList(data)) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =  PointViewHolder.create(parent)

}