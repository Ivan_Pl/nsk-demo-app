package com.pl.nskdemoapp.presentation.main

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainMvpView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showHomeScreen()

}