package com.pl.nskdemoapp.data.network.responces

import com.google.gson.annotations.SerializedName

class PointsResponse(@SerializedName("points") val points: List<PointEntity>) : BaseResponse()