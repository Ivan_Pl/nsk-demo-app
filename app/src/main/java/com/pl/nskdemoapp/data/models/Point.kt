package com.pl.nskdemoapp.data.models

data class Point(val x: Double,
                 val y: Double)