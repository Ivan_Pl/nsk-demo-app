package com.pl.nskdemoapp.data.repositories.points

import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.data.network.RemoteDataSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PointsRepository @Inject constructor(private val remoteDataSource: RemoteDataSource) {

    fun getPoints(count: Int) : Single<List<Point>> {
        return remoteDataSource.getPoints(count)
            .map { it.sortedBy { it.x } }
            .subscribeOn(Schedulers.io())
    }

}