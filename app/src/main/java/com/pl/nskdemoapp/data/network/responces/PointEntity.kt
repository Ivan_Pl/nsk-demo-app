package com.pl.nskdemoapp.data.network.responces

import com.google.gson.annotations.SerializedName

class PointEntity(@SerializedName("x") val x: Double,
                  @SerializedName("y") val y: Double)