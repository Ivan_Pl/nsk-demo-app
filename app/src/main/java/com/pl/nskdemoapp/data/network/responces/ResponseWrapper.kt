package com.pl.nskdemoapp.data.network.responces

import com.google.gson.annotations.SerializedName

class ResponseWrapper<T : BaseResponse>(@SerializedName("result") val result: Int = 0,
                                           @SerializedName("response") val response: T)