package com.pl.nskdemoapp.data.network

import com.pl.nskdemoapp.data.models.Point
import com.pl.nskdemoapp.data.network.responces.BaseResponse
import com.pl.nskdemoapp.data.network.responces.PointEntity
import com.pl.nskdemoapp.data.network.responces.PointsResponse
import com.pl.nskdemoapp.data.network.responces.ResponseWrapper
import com.pl.nskdemoapp.utils.errorHandling.ApiException
import io.reactivex.Single
import io.reactivex.SingleTransformer

class RemoteDataSourceImpl(private val api: ApiService) : RemoteDataSource {

    override fun getPoints(count: Int): Single<List<Point>> {
        return api.getPoints(count)
            .compose(handleResponseOrThrow<ResponseWrapper<PointsResponse>, List<PointEntity>> {
                (it as? PointsResponse)?.points ?: emptyList()
            })
            .map { it.map { entity -> Point(entity.x, entity.y) } }
    }

    private inline fun <T : ResponseWrapper<*>, R> handleResponseOrThrow(crossinline convert: (response: BaseResponse) -> R): SingleTransformer<T, R> {
        return SingleTransformer {
            it.map { wrapper ->
                if (wrapper.result == 0 && wrapper.response.message.isNullOrBlank() && wrapper.response.result == 0) {
                    convert(wrapper.response)
                } else {
                    throw ApiException.create(wrapper.response)
                }
            }
        }
    }

}