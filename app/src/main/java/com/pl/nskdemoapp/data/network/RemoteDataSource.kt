package com.pl.nskdemoapp.data.network

import com.pl.nskdemoapp.data.models.Point
import io.reactivex.Single

interface RemoteDataSource {

    fun getPoints(count: Int) : Single<List<Point>>

}