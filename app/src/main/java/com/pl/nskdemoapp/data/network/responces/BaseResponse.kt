package com.pl.nskdemoapp.data.network.responces

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @SerializedName("result") val result: Int = 0
    @SerializedName("message") val message: String? = null
}