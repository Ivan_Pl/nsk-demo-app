package com.pl.nskdemoapp.data.network

import com.pl.nskdemoapp.data.network.responces.PointsResponse
import com.pl.nskdemoapp.data.network.responces.ResponseWrapper
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    @FormUrlEncoded
    @POST("mobws/json/pointsList?version=1.1")
    fun getPoints(@Field("count") count: Int): Single<ResponseWrapper<PointsResponse>>

}